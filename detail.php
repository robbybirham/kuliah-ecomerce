<?php
include "conf/connect.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bongbae</title>
    
    <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/style.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>
<body>
    <!-- NAV & LOGO -->
    <div class="container">
        <div class="c3">
            <a href="index.html">
                <img src="images/logo.png" alt="logo" width="200px">
            </a>
        </div>

        <div class="c9">
            <ul class="nav">
                <li><a class="active" href="#home">Home</a></li>
                <li><a href="#news">Product</a></li>
                <li><a href="#news">Special</a></li>
                <li><a href="#contact">Promo</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#about">Cart <span class="count-cart">0</span></a></li>
                <li><a class="hovered" href="#about">Login</a></li>
            </ul>
        </div>
    </div>

    <div class="container">

        <div class="title-page">
            <h1>B-Tshirt</h1>
        </div>

        <?php
        $base_url = "http://localhost/kuliah/ecomerce/";

        //mengambil data menggunakan ID
        $id = $_GET['id'];
        
        //koneksi
        include "conf/connect.php";

        //query menampilkan data berdasarkan ID
        $query = mysqli_query($con,"SELECT * from barang WHERE id=$id");
        $data = mysqli_fetch_array($query);
        ?>
        <form action="add_to_cart.php" method="post">
        <input type="hidden" name="id" value="<?= $_GET['id']; ?>">
        <table>
            <tr>
                <td>Gambar</td>
                <td>: <img src="images/<?= $data['foto'] ?>" alt="" width="200px"></td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>: <?php echo $data['nama']; ?></td>
            </tr>
            <tr>
                <td>Harga</td>
                <td>: <?php echo $data['harga']; ?></td>
            </tr>
            <tr>
                <td>Jumlah</td>
                <td>: <input type="number" name="qty" id=""></td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td>: <?php echo $data['keterangan']; ?></td>
            </tr>
            <tr>
                <td></td>
                <td><button>BELI SEKARANG</button></td>
            </tr>
        </table>
        </form>
    </div>


    <footer>
        <hr>
        <center>
            <h5>Made with <img src="images/love.png" alt=""> by <b>Robby Birham</b></h5>
        </center>
    </footer>
</body>
</html>