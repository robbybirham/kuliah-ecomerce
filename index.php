<?php
include "conf/connect.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bongbae</title>
    
    <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/style.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>
<body>
    <!-- NAV & LOGO -->
    <div class="container">
        <div class="c3">
            <a href="index.html">
                <img src="images/logo.png" alt="logo" width="200px">
            </a>
        </div>

        <div class="c9">
            <ul class="nav">
                <li><a class="active" href="#home">Home</a></li>
                <li><a href="#news">Product</a></li>
                <li><a href="#news">Special</a></li>
                <li><a href="#contact">Promo</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#about">Cart <span class="count-cart">0</span></a></li>
                <li><a class="hovered" href="#about">Login</a></li>
            </ul>
        </div>
    </div>

    <div class="container">
        <img src="images/slide.png" width="100%" alt="">
    </div>

    <div class="container">
        <div class="title-page">
            <h1>B-Tshirt</h1>
        </div>

        <?php
        $base_url = "http://localhost/kuliah/ecomerce/";
        //menampilkan data
        $data = mysqli_query($con,"SELECT * from barang");
        while($datanya = mysqli_fetch_array($data))
        {
        ?>
        <div class="product">
            <img src="images/<?= $datanya['foto']; ?>" alt="" width="100%" class="product-img">

            <div class="product-desc">
                <h4><?= $datanya['nama']; ?></h4>
                <h5><?= $datanya['harga'] ?></h5>
                <a href="<?= $base_url ?>detail.php?id=<?= $datanya['id']; ?>"><button>BELI</button></a>
            </div>
        </div>
        <?php } ?>

        <!-- <div class="product">
            <img src="images/product-2.jpg" alt="" width="100%" class="product-img">

            <div class="product-desc">
                <h4>White tshirt by Bongbae</h4>
                <h5>Rp. 599.999</h5>
                <button>BELI</button>
            </div>
        </div>

        <div class="product">
            <img src="images/product-3.jpg" alt="" width="100%" class="product-img">

            <div class="product-desc">
                <h4>White tshirt by Bongbae</h4>
                <h5>Rp. 599.999</h5>
                <button>BELI</button>
            </div>
        </div>

        <div class="product">
            <img src="images/product-4.jpg" alt="" width="100%" class="product-img">

            <div class="product-desc">
                <h4>White tshirt by Bongbae</h4>
                <h5>Rp. 599.999</h5>
                <button>BELI</button>
            </div>
        </div> -->
    </div>
    
    <div class="container">
        <hr>
        <center>
            <img src="images/testi.png" alt="" width="200px;" class="testi-img">
            <h4><b>Mrs. Paryati</b></h4>
            <h3>"Everything can be fun at bongbae"</h3>
        </center>
    </div>

    <footer>
        <hr>
        <center>
            <h5>Made with <img src="images/love.png" alt=""> by <b>Robby Birham</b></h5>
        </center>
    </footer>
</body>
</html>