<h3>PENJUALAN TOKO BONGBAE</h3>
<table border="1" cellspacing="0" cellpadding="15">
    <thead>
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Nomor Telepon</th>
        <th>Alamat</th>
        <th>Tanggal</th>
        <th>Total</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
        <?php
        include "conf/connect.php";
        //query menampilkan transaksi terbaru
        $data = mysqli_query($con,"SELECT * from penjualan WHERE id='$_GET[id]' order by tanggal DESC");
        $no = 1;
        $datatransaksi = mysqli_fetch_array($data)
        ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= $datatransaksi['nama']; ?></td>
            <td><?= $datatransaksi['email']; ?></td>
            <td><?= $datatransaksi['nomor_telepon']; ?></td>
            <td><?= $datatransaksi['alamat']; ?></td>
            <td><?= $datatransaksi['tanggal']; ?></td>
            <td><?= $datatransaksi['total']; ?></td>
            <td><?= $datatransaksi['status']; ?></td>
        </tr>
    </tbody>
</table>

<h4>Detail Pembelian</h4>
<table border="1" cellspacing="0" cellpadding="15">
    <thead>
    <tr>
        <th>No</th>
        <th>Nama Barang</th>
        <th>Qty</th>
        <th>Harga</th>
        <th>Subtotal</th>
    </tr>
    </thead>
    <tbody>
        <?php
        include "conf/connect.php";
        //query menampilkan transaksi terbaru
        $query_detail = mysqli_query($con,"SELECT * from detail_penjualan WHERE penjualan_id='$_GET[id]'");

        $no = 1;$total = 0;
        while($datadetail = mysqli_fetch_array($query_detail))
        {
             //menampilkan data barang melalui database
             $barang = mysqli_query($con, "SELECT*from barang WHERE id='$datadetail[barang_id]'");
             $databarang = mysqli_fetch_array($barang);

             $total += $databarang['harga']*$datadetail['qty'];
        ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= $databarang['nama']; ?></td>
            <td><?= $datadetail['qty']; ?></td>
            <td><?= $databarang['harga']; ?></td>
            <td><?= $databarang['harga']*$datadetail['qty']; ?></td>
        </tr>
        <?php } ?>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>Total</td>
            <td><?= $total; ?></td>
        </tr>
    </tbody>
</table>