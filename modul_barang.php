<?php
    $base = "http://localhost/kuliah/ecomerce";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Input Barang</title>
    <link rel="stylesheet" href="assets/css/style.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>
<body>
   <div class="container">
   <h4>Modul Barang</h4>
    

   <hr>
   <a href="<?= $base.'/barang/input.php'; ?>"><button type="button">Tambah Barang</button></a>
   <table width="100%" border="1" cellspacing="0" cellpadding="5">
        <thead>
            <tr>
                <td width="10">No.</td>
                <td>Nama</td>
                <td>Harga</td>
                <td>Deskripsi</td>
                <td>Jumlah</td>
                <td width="100">Aksi</td>
            </tr>
        </thead>
        <tbody>
            <?php
            include "conf/connect.php";
            
            $no = 1;
            $data_query = mysqli_query($con,"SELECT * from barang ORDER BY nama");
            while($data = mysqli_fetch_array($data_query))
            {
            ?>
            <tr>
                <td><?= $no++; ?></td>
                <td><?= $data['nama']; ?></td>
                <td><?= $data['harga']; ?></td>
                <td><?= $data['keterangan']; ?></td>
                <td><?= $data['jumlah']; ?></td>
                <td>
                    <a href="<?= $base.'/barang/edit.php' ?>"><button type="button">edit</button></a>
                    <a href="<?= $base.'/barang/delete.php'."?id=$data[id]" ?>"><button type="button">delete</button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
   </table>
   </div>
</body>
</html>