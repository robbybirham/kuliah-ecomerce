<?php
include "conf/connect.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bongbae</title>
    
    <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/style.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>
<body>
    <!-- NAV & LOGO -->
    <div class="container">
        <div class="c3">
            <a href="index.html">
                <img src="images/logo.png" alt="logo" width="200px">
            </a>
        </div>

        <div class="c9">
            <ul class="nav">
                <li><a class="active" href="#home">Home</a></li>
                <li><a href="#news">Product</a></li>
                <li><a href="#news">Special</a></li>
                <li><a href="#contact">Promo</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#about">Cart <span class="count-cart">0</span></a></li>
                <li><a class="hovered" href="#about">Login</a></li>
            </ul>
        </div>
    </div>

    <div class="container">
        <div class="title-page">
            <h1>Keranjang</h1>
        </div>
        <?php
        $base_url = "http://localhost/kuliah/ecomerce/";
        //koneksi
        include "conf/connect.php";
        ?>
        <form action="proses_transaksi.php" method="post">
        <table>
            <tr>
                <td>Nama</td>
                <td>: <input type="text" name="nama" id="nama"></td>
            </tr>
            <tr>
                <td>Nomor Telephone</td>
                <td>: <input type="text" name="telepon" id="telepon"></td>
            </tr>
            <tr>
                <td>Email</td>
                <td>: <input type="text" name="email" id="email"></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>: <textarea name="alamat" id="alamat"></textarea></td>
            </tr>
        </table>

        <table width="100%" border="1" cellspacing="0" cellpadding="5">
        <thead>
            <tr>
                <td width="10">No.</td>
                <td>Nama</td>
                <td>Harga</td>
                <td>Deskripsi</td>
                <td>Jumlah</td>
                <td>Subtotal</td>
            </tr>
        </thead>
        <?php
        //memulai session
        session_start();
        //mengambil data keranjang
        $keranjang = $_SESSION['cart'];

        //koneksi
        include "conf/connect.php";
        
        $no = 1; $total = 0;
        foreach ($keranjang as $id => $qty) {
            //menampilkan data barang melalui database
            $barang = mysqli_query($con, "SELECT*from barang WHERE id='$id'");
            $databarang = mysqli_fetch_array($barang);

            //total belanja
            $total += $databarang['harga']*$qty;
            ?>
            <tr>
                <td><?= $no++; ?></td>
                <td><?= $databarang['nama']; ?></td>
                <td>Rp. <?= number_format($databarang['harga'],0,0,'.'); ?></td>
                <td><?= $databarang['keterangan']; ?></td>
                <td><?= $qty; ?></td>
                <td>Rp. <?= number_format($databarang['harga']*$qty,0,0,'.'); ?></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>Total</b></td>
            <td>Rp. <?= number_format($total,0,0,"."); ?></td>
        </tr>
   </table>
        
        <div>
        <center>
            <button type="submit">LANJUTKAN PROSES TRANSAKSI</button>
            </center>
        </div>
    </div>

    </form>


    <footer>
        <hr>
        <center>
            <h5>Made with <img src="images/love.png" alt=""> by <b>Robby Birham</b></h5>
        </center>
    </footer>
</body>
</html>