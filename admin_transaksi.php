<h3>PENJUALAN TOKO BONGBAE</h3>
<table border="1" cellspacing="0" cellpadding="15">
    <thead>
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Nomor Telepon</th>
        <th>Alamat</th>
        <th>Tanggal</th>
        <th>Total</th>
        <th>Status</th>
        <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
        <?php
        include "conf/connect.php";
        //query menampilkan transaksi terbaru
        $data = mysqli_query($con,"SELECT * from penjualan order by tanggal DESC");
        $no = 1;
        while($datatransaksi = mysqli_fetch_array($data))
        {
            ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= $datatransaksi['nama']; ?></td>
            <td><?= $datatransaksi['email']; ?></td>
            <td><?= $datatransaksi['nomor_telepon']; ?></td>
            <td><?= $datatransaksi['alamat']; ?></td>
            <td><?= $datatransaksi['tanggal']; ?></td>
            <td><?= $datatransaksi['total']; ?></td>
            <td><?= $datatransaksi['status']; ?></td>
            <td><a href="detail_penjualan.php?id=<?= $datatransaksi['id']; ?>">DETAIL</a></td>
        </tr>
        <?php
        }
        ?>
    </tbody>
</table>